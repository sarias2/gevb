<?php
  if(isset($_POST['mensaje']) && isset($_POST['email'])){
    $nombre = $_POST['nombre'];
    $telefono = $_POST['telefono'];
    $email = $_POST['email'];
    $tipo = $_POST['tipo'];
    $mensaje = $_POST['mensaje'];
    $para = 'contacto@gevirtualbook.com';
    $subject = "Contacto GEVB - ".$tipo;
    $cuerpo = 'Nombre: '.$nombre.'
    Tel: '.$telefono.'
    Email: '.$email.'
    Mensaje: '.$mensaje;
    $headers = "MIME-Version: 1.0 \r\n";
    $headers .= "From: ".$nombre." <".$email.">\r\n";
    $headers .= "Reply-To: ".$email."\r\n";
    $headers .= "Content-type: text/plain; charset=utf-8";

    $send = mail($para, $subject, $cuerpo, $headers);

    if($send){
      echo "<div class='alert alert-primary desaparece' role='alert'>";
      echo "Mensaje enviado";
      echo "</div>";
    } else {
      echo "Por favor inténtalo de nuevo";
    }
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <link rel="icon" type="image/png" href="img/Wireframe.png" />
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script>
              new WOW().init();
              </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="css/styles.css">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <script src="js/script.js" charset="utf-8"></script>
    <title>Contáctenos | GEVB</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light">
      <a class="navbar-brand" href="index.html"><img src="img/1x/logoHorizontal.png" alt="GEVB"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="nav navbar-nav ml-auto">
          <li class="nav-item ">
            <a class="nav-link" href="index.html">Inicio</a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="nosotros.html">Nosotros</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="servicios.html">Servicios</a>
          </li>
          <li class="nav-item" data-toggle="tooltip" data-placement="bottom" title="Próximamente">
            <a class="nav-link disabled" href="#">Pagos</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="contactenos.php">Contáctenos</a>
          </li>
          </ul>
        </div>
      </nav>

      <!-- <div id="carouselExampleSlidesOnly" class="carousel slide wow bounceInUp" data-ride="carousel">
        <div class="titulo wow fadeIn un-segundo">
          <h1>Grupo Educativo Virtual Book</h1>
          <p>Espacios virtuales de aprendizaje</p>
        </div>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="img/portada1.jpg" alt="GEVB">
          </div>
        </div>
      </div> -->

      <div class="alertaSC">
        <p>
          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
            <i class="fa fa-user" aria-hidden="true"></i><br>
            Servicio al Cliente <br>
              <i class="fa fa-angle-down"></i>
          </a>
        </p>
        <div class="collapse" id="collapseExample">
          <div class="card card-body">
            <ul class="listaSC">
              <li><a class="listaSC" href="#" data-toggle="tooltip" data-placement="bottom" title="Próximamente">GEVB en línea</a></li>
              <li><a class="listaSC" href="#" data-toggle="tooltip" data-placement="bottom" title="Próximamente">Preguntas frecuentes</a></li>
              <li><a class="listaSC" href="#" data-toggle="tooltip" data-placement="bottom" title="Próximamente">¿Quieres quejarte?</a></li>
            </ul>
          </div>
        </div>
        <!-- <div class="abrir bg-primary text-white" data-toggle="collapse" href="#multiCollapseExample1" aria-expanded="false" aria-controls="multiCollapseExample1" role="button">
          <p><strong>Servicio al Cliente</strong></p>
          <i class="fa fa-angle-down"></i>
        </div>
        <div class="collapse contenidoSC bg-primary text-white card card-body" id="multiCollapseExample1">
          <ul>
            <li><a href="#">GEVB en línea</a></li>
            <li><a href="#">Preguntas frecuentes</a></li>
            <li><a href="#">¿Quieres quejarte?</a></li>
          </ul>
        </div> -->
      </div>

      <div class="container">
        <div class="container">
          <div class="container">
          <br>
          <form class="" action="" method="post">
            <div class="form-group">
              <input type="text" name="nombre" class="form-control" id="" placeholder="Nombre completo"><br>
              <input type="number" name="telefono" class="form-control" id="" placeholder="Teléfono"><br>
              <input type="email" name="email" class="form-control" id="" placeholder="Email"><br>
              <select class="form-control" name="tipo">
                <option value="Información General">Información General</option>
                <option value="Consulta">Consulta</option>
                <option value="Queja">Queja</option>
                <option value="Soporte">Soporte</option>
              </select> <br>
              <textarea class="form-control" name="mensaje"placeholder="Escribe aquí tu mensaje" rows="8" cols="40"></textarea><br>
              <input class="btn btn-block bg-primary text-white" type="submit" name="" value="Enviar">
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="footer wow bounceInLeft">
      <img src="img/1x/gevb-blanco.png" alt="GEVB" class="logo-footer wow bounceIn un-segundo">
      <p class="wow fadeInDown dos-segundos">
        <strong>Espacios Virtuales de Aprendizaje</strong>
        <br>
        Grupo Educativo Virtual Book S.A.S. es un equipo de profesionales comprometidos con el mejoramiento de la educación en nuestro país a través de la incorporación de métodos de enseñanza acompañados con todas las posibilidades que brinda la tecnología.
      </p>

        <p class="wow fadeInDown dos-segundos">
          <a href="https://www.facebook.com/Grupo-Educativo-Virtual-Book-418246665258509/
" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>                                      <a href="contactenos.php" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i></a><br>
          Todos los derechos reservados <br>
          GEVB &#169; 2017 Copyright <br>
          <i class="fa fa-whatsapp" aria-hidden="true"></i> 310 426 0881 <br>
          Cali - Colombia <br>
          contacto@gevirtualbook.com
        </p>
    </div>
  </body>
</html>
